# SDG event simulator common
Common components for the sdg event simulators

- Flags: allows for breaking out of the game loop, and handle signals e.g. `SIGTERM`
- Event: common format for events
- Config: retrieve configuration from the environment
- Chance: chance calculation helper
- Bevy
 - run_loop: Runs the main loop 
 - plugins / resources: Provides the Clock and Nats connection