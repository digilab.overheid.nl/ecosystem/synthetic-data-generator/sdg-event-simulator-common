use bevy::prelude::*;
use nats::Connection;

use crate::config::SdgConfig;

#[derive(Resource)]
pub struct NatsState {
    pub connection: Connection,
    pub uri: String,
}

impl NatsState {
    pub fn new(uri: String) -> Self {
        info!("Connecting to nats at: {}", uri);
        let connection = nats::connect(&uri).expect("Could not connect to nats");
        Self { connection, uri }
    }
}


pub struct NatsPlugin {pub uri:String}

impl NatsPlugin {
    pub fn new(uri: String) -> Self {
        Self {  uri }
    }
}

impl Default for NatsPlugin {
    fn default() -> Self {
        Self::new(SdgConfig::nats_uri())
    }
}


impl Plugin for NatsPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(NatsState::new(self.uri.clone()));
    }
}