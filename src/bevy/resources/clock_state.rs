use bevy::prelude::*;
use chrono::{Duration, NaiveDateTime};

use crate::config::SdgConfig;

#[derive(Resource)]
pub struct ClockState {
    pub now: Option<NaiveDateTime>,
    pub time_per_tick: Duration,
}

impl Default for ClockState {
    fn default() -> Self {
        info!("Initializing clock state",);

        Self {
            now: None,
            time_per_tick: Duration::seconds(0),
        }
    }
}

impl ClockState {
    pub fn set_time(&mut self, t: &str) {
        let next = NaiveDateTime::parse_from_str(t, SdgConfig::TIMEFORMAT).unwrap();
        let now = match self.now {
            Some(v) => v,
            None => next,
        };

        if next.lt(&now) {
            warn!("Step back in time, refusing");
        } else {
            let step = next.signed_duration_since(now);
            self.now = Some(next);

            if self.time_per_tick != step {
                warn!(
                    "Step size changed {} -> {}",
                    self.time_per_tick.num_seconds(),
                    step.num_seconds()
                );
            }
            self.time_per_tick = step;

            debug!("TIME: {} - {:?}", now, self.time_per_tick);

            if now.format("%d %H:%M:%S").to_string() == "01 00:00:00" {
                info!("Start of month {}", now.format(SdgConfig::TIMEFORMAT))
            }
        }
    }

    pub fn get_time(&self) -> NaiveDateTime {
        self.now.unwrap()
    }
}

#[derive(Default)]
pub struct ClockPlugin {}

impl Plugin for ClockPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(ClockState::default());
    }
}
