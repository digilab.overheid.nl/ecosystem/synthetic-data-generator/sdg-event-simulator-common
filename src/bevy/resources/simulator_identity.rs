use bevy::prelude::*;

#[derive(Resource)]
pub struct SimulatorIdentity {
    name: String,
}

impl SimulatorIdentity {
    pub fn new<T: AsRef<str>>(name: T) -> Self {
        Self {
            name: name.as_ref().to_string(),
        }
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }
}
