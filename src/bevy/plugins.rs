use bevy::{app::PluginGroupBuilder, diagnostic::DiagnosticsPlugin, prelude::*};
use bevy_turborand::prelude::*;

use super::resources::{nats_state::NatsPlugin, clock_state::ClockPlugin};

pub struct SdgPluginGroup;

impl PluginGroup for SdgPluginGroup {
    fn build(self) -> PluginGroupBuilder {
        let rng_seed = 1337;

        PluginGroupBuilder::start::<Self>()
            .add(RngPlugin::new().with_rng_seed(rng_seed))
            .add(TaskPoolPlugin::default())
            .add(TypeRegistrationPlugin)
            .add(DiagnosticsPlugin)
            .add(NatsPlugin::default())
            .add(ClockPlugin::default())
    }
}
