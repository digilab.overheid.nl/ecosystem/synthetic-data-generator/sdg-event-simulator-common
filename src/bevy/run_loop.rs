use std::sync::atomic::Ordering::Relaxed;
use std::thread;

use bevy::prelude::App;
use nats::{
    jetstream::{BatchOptions, ConsumerConfig, PullSubscribeOptions, PullSubscription},
    Message,
};
use tracing::debug;
use tracing::warn;

use crate::{
    bevy::resources::{
        clock_state::ClockState, nats_state::NatsState, simulator_identity::SimulatorIdentity,
    },
    flags::{FLAG_RUN, RUN_FLAG},
};

pub fn run_loop(mut app: App) {
    let sim_ident = app.world.get_resource::<SimulatorIdentity>().unwrap();
    let nats_connection = app.world.get_resource::<NatsState>().unwrap();
    let time_stream = get_stream(&nats_connection.uri, sim_ident.get_name());

    loop {
        if RUN_FLAG.load(Relaxed) != FLAG_RUN {
            break;
        }

        let time = get_time(&time_stream);

        match time {
            TimeRes::Some(time, msg) => {
                debug!("Time is {}", time);

                let mut counter_state = app.world.get_resource_mut::<ClockState>().unwrap();
                counter_state.set_time(&time);

                app.update();

                msg.ack().unwrap();
            }
            TimeRes::None => thread::sleep(std::time::Duration::from_millis(100)),
            TimeRes::Error(e) => panic!("Failed to get time: {}", e),
        }
    }
}

fn get_stream(nats_uri: &str, identity: &str) -> PullSubscription {
    let tnc = nats::connect(nats_uri).expect("Could not connect to nats");

    let options = nats::JetStreamOptions::new();
    let stream = nats::jetstream::JetStream::new(tnc, options);

    stream
        .pull_subscribe_with_options(
            "time",
            &PullSubscribeOptions::new().consumer_config(ConsumerConfig {
                durable_name: Some(identity.to_string()),
                deliver_policy: nats::jetstream::DeliverPolicy::All,
                replay_policy: nats::jetstream::ReplayPolicy::Instant,
                ..Default::default()
            }),
        )
        .unwrap()
}

enum TimeRes {
    Some(String, Message),
    None,
    Error(std::io::Error),
}

fn get_time(s: &PullSubscription) -> TimeRes {
    let time = s.fetch(BatchOptions {
        batch: 1,
        expires: None,
        no_wait: true,
    });

    match time {
        Ok(mut t) => {
            let message = t.next();
            if let Some(message) = message {
                let res = std::str::from_utf8(&message.data).unwrap().to_string();
                TimeRes::Some(res, message)
            } else {
                TimeRes::None
            }
        }
        Err(e) => {
            match e.kind() {
                std::io::ErrorKind::TimedOut => warn!("Time timed out"),
                std::io::ErrorKind::Other => warn!("Time disconnected"),
                _ => todo!(),
            };
            TimeRes::Error(e)
        }
    }
}
