use std::{sync::atomic::{AtomicU16, Ordering::Relaxed}, thread, time};
use signal_hook::{consts::*, iterator::Signals};
use tracing::info;


pub static RUN_FLAG: AtomicU16 = AtomicU16::new(FLAG_RUN);
pub const FLAG_RUN: u16 = 0;
pub const FLAG_COMPLETE: u16 = 1;
pub const FLAG_QUIT: u16 = 2;

pub fn setup_signals() {
    let mut signals = Signals::new([SIGINT, SIGQUIT, SIGTERM]).unwrap();

    thread::spawn(move || {
        for sig in signals.forever() {
            info!("Received signal {:?}, setting end flag", sig);
            RUN_FLAG.store(FLAG_QUIT, Relaxed);
        }
    });
}

pub fn wait_for_exit() {
    if RUN_FLAG.load(Relaxed) == FLAG_QUIT {
        info!("End flag set, exiting");
        return;
    }

    info!("Waiting for end flag");
    loop {
        if RUN_FLAG.load(Relaxed) == FLAG_QUIT {
            info!("End flag set, ending wait");
            break;
        } else {
            thread::sleep(time::Duration::from_secs(1));
        }
    }
}
