use std::env;

pub struct SdgConfig {}

impl SdgConfig {
    pub const TIMEFORMAT: &'static str = "%Y-%m-%dT%H:%M:%S.00Z";

    pub fn nats_uri() -> String {
        let nats_host =
            env::var("SDG_NATS_SERVICE_HOST").expect("Missing SDG_NATS_SERVICE_HOST from env");
        let nats_port =
            env::var("SDG_NATS_SERVICE_PORT").expect("Missing SDG_NATS_SERVICE_PORT from env");
        format!("nats://{}:{}", &nats_host, &nats_port)
    }
}

impl Default for SdgConfig {
    fn default() -> Self {
        let _ = env::var("SDG_NATS_SERVICE_HOST").expect("Missing SDG_NATS_SERVICE_HOST from env");
        let _ = env::var("SDG_NATS_SERVICE_PORT").expect("Missing SDG_NATS_SERVICE_PORT from env");
        Self {}
    }
}
