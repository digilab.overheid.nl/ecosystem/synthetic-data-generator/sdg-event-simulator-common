use serde::{Serialize, Deserialize};
use uuid::Uuid;
use serde_json::Value;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Event {
    pub id: Uuid,
    #[serde(rename = "occurredAt")]
    pub occured_at: String,
    #[serde(rename = "registeredAt")]
    pub registered_at: String,
    #[serde(rename = "subjectIds")]
    pub subject_ids: Vec<Uuid>,
    #[serde(rename = "eventType")]
    pub event_type: String,
    #[serde(rename = "eventData")]
    pub event_data: Value,
}
