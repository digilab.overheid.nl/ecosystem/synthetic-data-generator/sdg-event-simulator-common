use chrono::Duration;
use num_traits::{FromPrimitive, ToPrimitive};
use rust_decimal::Decimal;
use tracing::debug;

#[derive(Debug, Clone)]
pub struct PercentageChance {
    chance: Decimal,
}

impl PercentageChance {
    /// Create a new `PercentageChance` based on a percentage chance
    pub fn new(percentage: f64) -> Self {
        let numerator = Decimal::from_f64(percentage).unwrap();
        let denominator = Decimal::from_u64(100).unwrap();

        let chance = numerator.checked_div(denominator).unwrap();
        Self { chance }
    }

    /// Scale a `PercentageChance` from `source_period` to `sim_period`
    ///
    /// # Example
    /// scale a 10% chance over 24 hours -> a 5% chance over 12 hours
    /// ```
    /// use sdg_event_simulator_common::chance::PercentageChance;
    /// let day = PercentageChance::new(10.0);
    /// assert_eq!(day.fraction_of_u64(), u64::MAX / 10);
    ///
    /// let half = day.scale(chrono::Duration::hours(24), chrono::Duration::hours(12));
    /// assert_eq!(half.fraction_of_u64(), u64::MAX / 10 / 2);
    /// ```
    pub fn scale(mut self, source_period: Duration, sim_period: Duration) -> Self {
        let sim_period = Decimal::from_i64(sim_period.num_seconds()).unwrap();
        let source_period = Decimal::from_i64(source_period.num_seconds()).unwrap();

        debug!("Time source: {:?} sim: {:?}", source_period, sim_period);

        let period_scale = sim_period.checked_div(source_period).unwrap();

        debug!("Scale: {:?}", period_scale);

        let chance = self.chance.checked_mul(period_scale).unwrap();

        debug!("Chance source: {} sim: {}", self.chance, chance);

        self.chance = chance;
        self
    }

    pub fn fraction_of_u64(&self) -> u64 {
        let f = Decimal::from_u64(u64::MAX)
            .map(|v| v.saturating_mul(self.chance))
            .map(|v| {
                if v > u64::MAX.into() {
                    Some(u64::MAX)
                } else {
                    v.to_u64()
                }
            });

        match f {
            Some(Some(v)) => v,
            _ => panic!("Could not get fraction `{:?}`", self.chance.unpack()),
        }
    }
}

#[cfg(test)]
mod test {
    use super::{Duration, PercentageChance};
    use num_traits::ToPrimitive;
    use rust_decimal::{prelude::FromPrimitive, Decimal};

    // Allow for a 0.1 percent error margin
    const ERROR_MARGIN: u64 = u64::MAX / 1000;

    impl PercentageChance {
        pub fn get(&self) -> Decimal {
            self.chance
        }
    }

    fn diff_u64(a: u64, b: u64) -> u64 {
        if a > b {
            a - b
        } else {
            b - a
        }
    }

    fn diff_dec(a: Decimal, b: Decimal) -> Decimal {
        let over = a.saturating_sub(b);
        let under = b.saturating_sub(a);

        if over > under {
            over
        } else {
            under
        }
    }

    #[test]
    fn test_scaling_manual() {
        let base = 10.0;
        let c_q_d = PercentageChance::new(base).fraction_of_u64();
        let c_d = PercentageChance::new(base * 4.0)
            .scale(Duration::seconds(24), Duration::seconds(6))
            .fraction_of_u64();

        let c_h = PercentageChance::new(base / 6.0)
            .scale(Duration::seconds(1), Duration::seconds(6))
            .fraction_of_u64();

        let diff_d = diff_u64(c_q_d, c_d);
        assert!(diff_d < ERROR_MARGIN, "diff: {}", diff_d,);

        let diff_h = diff_u64(c_q_d, c_h);
        assert!(diff_h < ERROR_MARGIN, "diff: {}", diff_h);
    }

    #[test]
    fn test_scaling_loop() {
        let source_period = Duration::hours(24);
        let source_chance = PercentageChance::new(1.0);
        let margin = Decimal::from_u64(ERROR_MARGIN).unwrap();

        for p in 1u64..=48 {
            let sim_period = Duration::hours(p as i64);

            let sim_chance = source_chance.clone().scale(source_period, sim_period);

            let scale = Decimal::from_u64(24)
                .unwrap()
                .checked_div(p.into())
                .unwrap();

            let scaled_chance = sim_chance.get().checked_mul(scale).unwrap();

            let diff = diff_dec(source_chance.get(), scaled_chance);

            assert!(
                diff < margin,
                "p: {} diff: {}, 1 percent: {}",
                p,
                diff,
                margin
            )
        }
    }

    #[test]
    fn test_fraction_converion() {
        let source_chance = PercentageChance::new(50.0);
        let frac = Decimal::from_u64(u64::MAX.into())
            .unwrap()
            .checked_mul(source_chance.get())
            .unwrap();

        assert_eq!(frac.to_u64().unwrap(), u64::MAX / 2);
    }

    #[test]
    fn test_over_100_percent() {
        let c = PercentageChance::new(125.0);
        let f = c.fraction_of_u64();
        assert_eq!(f, u64::MAX);
    }
}
