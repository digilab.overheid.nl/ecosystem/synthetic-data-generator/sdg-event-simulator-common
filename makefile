.PHONY: lint
lint: 
	cargo install cargo-machete
	cargo machete
	cargo clippy

.PHONY: test
test:
	cargo test --all
